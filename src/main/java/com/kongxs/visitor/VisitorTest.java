package com.kongxs.visitor;

interface Visitor {
	public void visit(Subject sub);
}

class MyVisitor implements Visitor {
	@Override
	public void visit(Subject sub) {
		System.out.println("visit the subject：" + sub.getSubject());
	}
}
// Subject类，accept方法，接受将要访问它的对象，getSubject()获取将要被访问的属性，
interface Subject {
	public void accept(Visitor visitor);
	public String getSubject();
}

class MySubject implements Subject {
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	@Override
	public String getSubject() {
		return "love";
	}
}

public class VisitorTest {
	public static void main(String[] args) {
		Visitor visitor = new MyVisitor();
		Subject sub = new MySubject();
		sub.accept(visitor);
	}
}
