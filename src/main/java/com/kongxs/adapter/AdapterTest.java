package com.kongxs.adapter;

class Source {
	public void method1() {
		System.out.println("this is original method!");
	}
}

interface Targetable {
	/* 与原类中的方法相同 */
	public void method1();

	/* 新类的方法 */
	public void method2();
}

// Adapter类继承Source类，实现Targetable接口
class Adapter extends Source implements Targetable {
	@Override
	public void method2() {
		System.out.println("this is the targetable method!");
	}
}

class Wrapper implements Targetable {
	private Source source;

	public Wrapper(Source source) {
		super();
		this.source = source;
	}

	@Override
	public void method1() {
		source.method1();
	}

	@Override
	public void method2() {
		System.out.println("this is the targetable method!");
	}
}

interface Sourceable {
	public void method1();
	public void method2();
}

abstract class Wrapper2 implements Sourceable {
	public void method1() {}

	public void method2() {}
}

class SourceSub1 extends Wrapper2 {
	public void method1() {
		System.out.println("the sourceable interface's first Sub1!");
	}
}

class SourceSub2 extends Wrapper2 {
	public void method2() {
		System.out.println("the sourceable interface's second Sub2!");
	}
}

public class AdapterTest {

	public static void main(String[] args) {
		// 类的适配器模式
		Targetable target = new Adapter();
		target.method1();
		target.method2();

		// 对象的适配器模式
		Source source = new Source();
		Targetable target1 = new Wrapper(source);
		target1.method1();
		target1.method2();

		// 接口的适配器模式
		Sourceable source1 = new SourceSub1();
		Sourceable source2 = new SourceSub2();
		source1.method1();
		source1.method2();
		source2.method1();
		source2.method2();
	}

}
