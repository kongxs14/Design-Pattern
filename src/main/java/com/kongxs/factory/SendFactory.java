package com.kongxs.factory;

interface Sender {
	public void Send();
}

class MailSender implements Sender {
	@Override
	public void Send() {
		System.out.println("this is mailsender!");
	}
}

class SmsSender implements Sender {
	@Override
	public void Send() {
		System.out.println("this is sms sender!");
	}
}

public class SendFactory {

	public Sender produce(String type) {
		if ("mail".equals(type)) {
			return new MailSender();
		} else if ("sms".equals(type)) {
			return new SmsSender();
		} else {
			System.out.println("请输入正确的类型!");
			return null;
		}
	}

	public static Sender produceMail() {
		return new MailSender();
	}

	public static Sender produceSms() {
		return new SmsSender();
	}

	public static void main(String[] args) {
		SendFactory factory = new SendFactory();
		// 普通工厂模式
		Sender sender = factory.produce("sms");
		sender.Send();

		// 多个工厂方法模式
		Sender sender1 = factory.produceMail();
		sender1.Send();

		// 静态工厂方法模式
		Sender sender2 = SendFactory.produceMail();
		sender2.Send();
	}

}
