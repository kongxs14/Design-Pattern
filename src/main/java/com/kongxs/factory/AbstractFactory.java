package com.kongxs.factory;

interface Provider {
	public Sender produce();
}

class SendMailFactory implements Provider {

	@Override
	public Sender produce() {
		return new MailSender();
	}
}

class SendSmsFactory implements Provider {

	@Override
	public Sender produce() {
		return new SmsSender();
	}
}

public class AbstractFactory {

	public static void main(String[] args) {
		Provider provider = new SendMailFactory();
		Sender sender = provider.produce();
		sender.Send();
	}

}
